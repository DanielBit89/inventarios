﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Inventarios.Models
{
    [MetadataType(typeof (Blogs_Validation))]

    public partial class Blogs
    {
    }

    public class Blogs_Validation
    {
        public int BlogId { get; set; }
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "El campo nombre es obligatorio")]
        public string Name { get; set; }
        public string Url { get; set; }
    }
}