﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Inventarios.Models    
{
    [MetadataType(typeof(Posts_Validation))]
        
    public partial class Posts
    {
    }

    public class Posts_Validation
    {
        public int PostId { get; set; }
        [Display(Name = "Titulo")]
        [StringLength(200,ErrorMessage = "Rebasó el número de caracteres")]
        public string Title { get; set; }
        [Display(Name = "Contenido")]
        public string Content { get; set; }
        public int BlogId { get; set; }
    }
}