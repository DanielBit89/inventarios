﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Inventarios.Startup))]
namespace Inventarios
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
